## Planification des rythmes de travail ##

[![Build Status](https://drone.io/bitbucket.org/cantico/workschedules/status.png)](https://drone.io/bitbucket.org/cantico/workschedules/latest)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/cantico/workschedules/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/cantico/workschedules/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/b/cantico/workschedules/badges/coverage.png?b=master)](https://scrutinizer-ci.com/b/cantico/workschedules/?branch=master)