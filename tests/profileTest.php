<?php


require_once dirname(__FILE__).'/mock/functions.php';
require_once dirname(__FILE__).'/../programs/set/profile.class.php';
require_once dirname(__FILE__).'/../programs/set/rule.class.php';


class workschedules_ProfileTest extends PHPUnit_Framework_TestCase
{
    
    /**
     * @return workschedules_Profile
     */
    protected function getMockBaseProfile()
    {
        $set = new workschedules_ProfileSet();
        
        $profile = $set->newRecord();
        $profile->id = 1;
        $profile->name = 'Mock profile';
        $profile->user = 0;
        
        return $profile;
    }
    
    
    
    /**
     * @return workschedules_Profile
     */
    protected function getMockProfile($rrule)
    {
        $profile = $this->getMockBaseProfile();
        $ruleSet = $profile->ruleSet();
        
        $mockBackend = $profile->getParentSet()->getBackend();
        /* @var $mockBackend ORM_MySqlMockBackend */
        
        // a basic rule
        
        $rule = $ruleSet->newRecord();
        $rule->begin = '2014-12-01 09:00:00';
        $rule->end = '2014-12-01 18:00:00';
        $rule->exclude = 0;
        $rule->profile = $profile->id;
        $rule->rrule = $rrule;
        $rule->sortkey = 0;
        
        $mockBackend->setSelectReturn($ruleSet, $ruleSet->profile->is($profile->id), array($rule));
        $mockBackend->setSelectReturn($ruleSet, $ruleSet->profile->is($profile->id)->_AND_($ruleSet->exclude->is(0)), array($rule));
        
        return $profile;
    }
    
    
    
    
    
    /**
     * @return workschedules_Profile
     */
    protected function getMock2RulesProfile()
    {
        $profile = $this->getMockBaseProfile();
        $ruleSet = $profile->ruleSet();
    
        $mockBackend = $profile->getParentSet()->getBackend();
        /* @var $mockBackend ORM_MySqlMockBackend */
    
        // a basic rule
    
        $rule1 = $ruleSet->newRecord();
        $rule1->begin = '2014-12-01 09:00:00';
        $rule1->end = '2014-12-01 12:00:00';
        $rule1->exclude = 0;
        $rule1->profile = $profile->id;
        $rule1->rrule = 'INTERVAL=1;FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR';
        $rule1->sortkey = 0;
        
        $rule2 = $ruleSet->newRecord();
        $rule2->begin = '2014-12-01 13:00:00';
        $rule2->end = '2014-12-01 18:00:00';
        $rule2->exclude = 0;
        $rule2->profile = $profile->id;
        $rule2->rrule = 'INTERVAL=1;FREQ=WEEKLY;BYDAY=MO,TU,TH,FR';
        $rule2->sortkey = 0;
    
        $mockBackend->setSelectReturn($ruleSet, $ruleSet->profile->is($profile->id), array($rule1, $rule2));
        $mockBackend->setSelectReturn($ruleSet, $ruleSet->profile->is($profile->id)->_AND_($ruleSet->exclude->is(0)), array($rule1, $rule2));
    
        return $profile;
    }
    
    
    
    
    
    
    /**
     * @return workschedules_Profile
     */
    protected function getMockProfileWithExclusion()
    {
        $profile = $this->getMockBaseProfile();
        $ruleSet = $profile->ruleSet();
    
        $mockBackend = $profile->getParentSet()->getBackend();
        /* @var $mockBackend ORM_MySqlMockBackend */
    
        // a basic rule
    
        $rule1 = $ruleSet->newRecord();
        $rule1->begin = '2014-12-01 09:00:00';
        $rule1->end = '2014-12-01 18:00:00';
        $rule1->exclude = 0;
        $rule1->profile = $profile->id;
        $rule1->rrule = 'INTERVAL=1;FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR';
        $rule1->sortkey = 0;
    
        $rule2 = $ruleSet->newRecord();
        $rule2->begin = '2014-12-01 12:00:00';
        $rule2->end = '2014-12-01 18:00:00';
        $rule2->exclude = 1;
        $rule2->profile = $profile->id;
        $rule2->rrule = 'INTERVAL=1;FREQ=WEEKLY;BYDAY=WE';
        $rule2->sortkey = 0;
    
        $mockBackend->setSelectReturn($ruleSet, $ruleSet->profile->is($profile->id), array($rule1, $rule2));
        $mockBackend->setSelectReturn($ruleSet, $ruleSet->profile->is($profile->id)->_AND_($ruleSet->exclude->is(0)), array($rule1));
        $mockBackend->setSelectReturn($ruleSet, $ruleSet->profile->is($profile->id)->_AND_($ruleSet->exclude->is(1)), array($rule2));
    
        return $profile;
    }
    
    
    
    
    
    
    
    public function testSelectRules()
    {
        $profile = $this->getMockProfile('INTERVAL=1;FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR');
        $list = $profile->selectRules();
        
        $this->assertEquals(1, $list->count());
    }
    
    
    public function testRRuleWeekPeriodSimple()
    {
        $profile = $this->getMockProfile('INTERVAL=1;FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR');
        
        $begin = new BAB_DateTime(2014, 12, 1, 0, 0, 0);
        $end = new BAB_DateTime(2014, 12, 5, 23, 59, 59);
        
        $periods = $profile->getRulesPeriods($begin, $end);
        
        $this->assertEquals(5, count($periods));
    }
    
    
    public function testRRuleWeekPeriodByDay()
    {
        $profile = $this->getMockProfile('INTERVAL=1;FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR');
    
        $begin = new BAB_DateTime(2014, 12, 1, 0, 0, 0);
        $end = new BAB_DateTime(2014, 12, 7, 23, 59, 59);
    
        $periods = $profile->getRulesPeriods($begin, $end);
    
        $this->assertEquals(5, count($periods));
    }
    
    
    public function testRRuleWeekPeriodFourDays()
    {
        $profile = $this->getMockProfile('INTERVAL=1;FREQ=WEEKLY;BYDAY=MO,TU,TH,FR');
    
        $begin = new BAB_DateTime(2014, 12, 1, 0, 0, 0);
        $end = new BAB_DateTime(2014, 12, 5, 23, 59, 59);
    
        $periods = $profile->getRulesPeriods($begin, $end);
    
        $this->assertEquals(4, count($periods));
    }
    
    
    public function testRRule2rules()
    {
        $profile = $this->getMock2RulesProfile();
    
        $begin = new BAB_DateTime(2014, 12, 2, 0, 0, 0);
        $end = new BAB_DateTime(2014, 12, 4, 12, 0, 0);
    
        $periods = $profile->getRulesPeriods($begin, $end);
    
        $this->assertEquals(4, count($periods));
    }
    
    
    public function testWithExclusion()
    {
        $profile = $this->getMockProfileWithExclusion();
    
        $begin = new BAB_DateTime(2014, 12, 1, 0, 0, 0);
        $end = new BAB_DateTime(2014, 12, 7, 23, 59, 59);
    
        $periods = $profile->getRulesPeriods($begin, $end);
    
        $this->assertEquals(5, count($periods));
        
        $monday = $periods[0];
        $tuesday = $periods[1];
        $wednesday = $periods[2];
        $thursday = $periods[3];
        $friday = $periods[4];
        
        $this->assertEquals('2014-12-01 09:00:00', $monday['start']->getIsoDateTime());
        $this->assertEquals('2014-12-01 12:00:00', $monday['end']->getIsoDateTime());
        
        $this->assertEquals('2014-12-02 09:00:00', $tuesday['start']->getIsoDateTime());
        $this->assertEquals('2014-12-02 18:00:00', $tuesday['end']->getIsoDateTime());
        
        $this->assertEquals('2014-12-03 09:00:00', $wednesday['start']->getIsoDateTime());
        $this->assertEquals('2014-12-03 12:00:00', $wednesday['end']->getIsoDateTime());
        
        $this->assertEquals('2014-12-04 09:00:00', $thursday['start']->getIsoDateTime());
        $this->assertEquals('2014-12-04 18:00:00', $thursday['end']->getIsoDateTime());
        
        $this->assertEquals('2014-12-05 09:00:00', $friday['start']->getIsoDateTime());
        $this->assertEquals('2014-12-05 18:00:00', $friday['end']->getIsoDateTime());
    }
}
