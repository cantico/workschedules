<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
workschedules_loadOrm();


/**
 * @property ORM_UserField				$user
 * @property workschedules_ProfileSet	$profile
 * @property ORM_DateField              $from
 * @property ORM_DateField              $to
 * 
 * @method workschedules_UserProfile[] select()
 * @method workschedules_UserProfile newRecord()
 * @method workschedules_UserProfile get()
 */
class workschedules_UserProfileSet extends ORM_MySqlRecordSet
{
	public function __construct()
	{
		parent::__construct();
		
		$this->setPrimaryKey('id');
	
		$this->addFields(
			ORM_UserField('user'),
			ORM_DateField('from'),
			ORM_DateField('to')
		);
		
		$this->hasOne('profile', 'workschedules_ProfileSet');

	}
	
	
	
	/**
	 * Saves a record.
	 * 
	 * @throws bab_SaveErrorException
	 *
	 * @param workschedules_UserProfile $userProfileRecord The record to save.
	 *
	 * @return boolean True on success, false otherwise.
	 */
	public function save(workschedules_UserProfile $userProfileRecord)
	{
	    $userProfileRecord->checkPeriod();
		$return = parent::save($userProfileRecord);
		
		require_once dirname(__FILE__).'/user.class.php';
		$userSet = new workschedules_UserSet();
		
		$user = $userSet->get($userSet->user->is($userProfileRecord->user));
		if (!isset($user)) {
		    $user = $userSet->newRecord();
		    $user->user = $userProfileRecord->user;
		    $user->username_copy = bab_getUserName($userProfileRecord->user);
		    $user->save();
		}
		
		workschedules_PeriodModified((int) $userProfileRecord->user);
		
		return $return;
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see ORM_RecordSet::delete()
	 */
	public function delete(ORM_Criteria $criteria)
	{
		$res = $this->select($criteria);
		foreach($res as $userProfile)
		{
			workschedules_PeriodModified((int) $userProfile->user);
		}
		
		return parent::delete($criteria);
	}
	
}

/**
 * @property int					$id
 * @property int					$user
 * @property string                 $from
 * @property string                 $to
 * @property workschedules_Profile	$profile
 *
 */
class workschedules_UserProfile extends ORM_MySqlRecord
{
	
	/**
	 * delete user custom rules or detach from the named profile
	 * @return bool
	 */
	public function clear()
	{
		$profile = $this->profile();
		
		if (!($profile instanceof workschedules_Profile))
		{
			// no profile? maybe deleted
			return false;
		}
		
		if ($profile->isNamedProfile())
		{
			$this->profile = 0;
			$this->save();
			return true;
		}
		
		// delete the user profile
		$setRule = new workschedules_RuleSet();
		$setRule->delete($setRule->profile->is($profile->id));
		
		$profileSet = new workschedules_ProfileSet();
		$profileSet->delete($profileSet->id->is($profile->id));
		
		return true;
	}

	
	/**
	 * @return false
	 */
	public function createCustomRulesProfile()
	{
		$profile = $this->profile();
		
		if ($profile instanceof workschedules_Profile)
		{
			if (!$profile->isNamedProfile())
			{
				// custom rules profile allready exists
				return true;
			}
		}
		
		$profileSet = new workschedules_ProfileSet();
		$profile = $profileSet->newRecord();
		$profile->disabled = 0;
		$profile->modifiedOn = date('Y-m-d H:i:s');
		if (!$profile->save())
		{
			return false;
		}
		
		$this->profile = $profile->id;
		
		return true;
	}
	
	/**
	 * Ensure period validity before saving
	 * @throws bab_SaveErrorException
	 */
	public function checkPeriod()
	{
	    if (!$this->user) {
	        throw new bab_SaveErrorException(workschedules_translate('User is mandatory'));
	    }

	    if($profileName = $this->getIntersecPeriode($this->from, $this->to)){
	        
	        require_once $GLOBALS['babInstallPath'].'utilit/controller.class.php';
	        
	        throw new bab_SaveErrorException(
	            sprintf(
	                workschedules_translate('The period is already covered by this profile: %s, only one profile can be assigned over a period.'),
	                $profileName
	            )
	        );
	    }

	    return true;
	}
	
	
	/**
	 * 
	 * @param string $from
	 * @param string $to
	 * 
	 * @return string
	 */
	public function getIntersecPeriode($from, $to)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		
		$set = new workschedules_UserProfileSet();
		$set->profile();
		$all = $set->select($set->user->is($this->user)->_AND_($set->id->notIn($this->id)));
		
		if($to == '0000-00-00'){
			$to = '9999-12-31';
		}
		
		foreach($all as $each){
			if($each->to == '0000-00-00'){
				$each->to = '9999-12-30';
			}
			if(BAB_DateTime::periodIntersect($from. ' 00:00:00', $to . ' 23:59:59', $each->from. ' 00:00:00', $each->to. ' 23:59:59')){
				return $each->profile->name;
			}
		}
		
		return null;
	}
}