<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * @property	ORM_BoolField				$exclude
 * @property	ORM_DatetimeField			$begin
 * @property	ORM_DatetimeField			$end
 * @property 	ORM_StringField				$rrule
 * @property	ORM_IntField				$sortkey
 * @property	workschedules_ProfileSet	$profile
 * 
 * @method workschedules_Rule newRecord()
 * @method workschedules_Rule[] select()
 * @method workschedules_Rule get()
 */
class workschedules_RuleSet extends ORM_MysqlRecordSet
{
	public function __construct()
	{
		parent::__construct();
	
		$this->setPrimaryKey('id');
	
		$this->addFields(
				ORM_BoolField('exclude'),
				ORM_DatetimeField('begin'),
				ORM_DatetimeField('end'),
				ORM_StringField('rrule'),
				ORM_IntField('sortkey')
		);
	
		$this->hasOne('profile', 'workschedules_ProfileSet');
	}
	
	
	/**
	 * Saves a record.
	 *
	 * @param ORM_Record $oRecord The record to save.
	 *
	 * @return boolean True on success, false otherwise.
	 */
	public function save(ORM_Record $oRecord)
	{
		$return = parent::save($oRecord);
	
		workschedules_PeriodModified();
	
	
		return $return;
	}
}

/**
 * @property	int							$exclude
 * @property	string						$begin
 * @property	string						$end
 * @property 	string						$rrule
 * @property	int							$sortkey
 * @property	workschedules_Profile		$profile
 */
class workschedules_Rule extends ORM_MysqlRecord
{
	
	/**
	 * A description of the initial period
	 * @return string
	 */
	public function getPeriodDescription()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		
		$begin = BAB_DateTime::fromIsoDateTime($this->begin);
		$end = BAB_DateTime::fromIsoDateTime($this->end);
		
		$hour_begin = sprintf('%02d:%02d', $begin->getHour(),$begin->getMinute());
		$hour_end = sprintf('%02d:%02d', $end->getHour(),$end->getMinute());
		
		$sameday = $begin->getIsoDate() === $end->getIsoDate();
		$end_midnight = (1 === BAB_DateTime::dateDiffIso($begin->getIsoDate(), $end->getIsoDate()) && '00:00' === $hour_end);
		
		
		if ($sameday || $end_midnight)
		{
			if ('00:00' === $hour_begin && (($sameday && '23:59' === $hour_end) || $end_midnight))
			{
				return sprintf(workschedules_translate('The %s all day'), $begin->longFormat(false));
			} 
			
			return sprintf(workschedules_translate('The %s from %s to %s'), $begin->longFormat(false), $hour_begin, $hour_end);
		}
		
		return sprintf(workschedules_translate('The  period from %s to %s'), $begin->longFormat(true), $end->longFormat(true));
	}
	
	
	/**
	 * 
	 * @param string $week_day
	 * @return string
	 */
	private function getWeekDay($week_day)
	{
	    switch($week_day)
	    {
	        case 'MO': return workschedules_translate('monday');
	        case 'TU': return workschedules_translate('thusday');
	        case 'WE': return workschedules_translate('wednesday');
	        case 'TH': return workschedules_translate('thursday');
	        case 'FR': return workschedules_translate('friday');
	        case 'SA': return workschedules_translate('saturday');
	        case 'SU': return workschedules_translate('sunday');
	    }
	    
	    return $week_day;
	}
	
	
	/**
	 * @return string
	 */
	public function rruleDescription()
	{
		require_once dirname(__FILE__).'/../RRule.php';
		
		//$begin = BAB_DateTime::fromIsoDateTime($this->begin);
		
		//$rrule = new workschedules_RRule($begin->getICal(true), $this->rrule);
		
		//return print_r($rrule->_part, true);
		
		if($this->rrule){
			$returnDescription = '';
			$arryRule = workschedules_getRRuleForm($this->rrule);
			switch($arryRule['repeat_frequency']){
				case 'weekly':
					
					if(isset($arryRule['repeat_wd'])){
						foreach($arryRule['repeat_wd'] as $k => $v)
						{
							$arryRule['repeat_wd'][$k] = $this->getWeekDay($v);
						}
						$returnDescription = workschedules_translate('the') . ' ' . implode(', ', $arryRule['repeat_wd']) . ' ';
					}
				
					if($arryRule['repeat_n_w'] != 1){
						$returnDescription.= sprintf(workschedules_translate('repeat each %s weaks'),$arryRule['repeat_n_w']);
					}else{
						$returnDescription.= workschedules_translate('repeat each weaks');
					}
		
					break;
		
		
				case 'monthly':
					if($arryRule['repeat_n_m'] != 1){
						$returnDescription.= sprintf(workschedules_translate('repeat each %s months'),$arryRule['repeat_n_m']);
					}else{
						$returnDescription.= workschedules_translate('repeat each months');
					}
					break;
		
				case 'yearly': /* yearly */
					if($arryRule['repeat_n_y'] != 1){
						$returnDescription.= sprintf(workschedules_translate('repeat each %s years'),$arryRule['repeat_n_y']);
					}else{
						$returnDescription.= workschedules_translate('repeat each years');
					}
					break;
		
				case 'daily': /* daily */
					if($arryRule['repeat_n_d'] != 1){
						$returnDescription.= sprintf(workschedules_translate('repeat each %s days'),$arryRule['repeat_n_d']);
					}else{
						$returnDescription.= workschedules_translate('repeat each days');
					}
					break;
			}
			
			if (isset($arryRule['repeat_end_cb']) && $arryRule['repeat_end_cb'] == 1)
			{
				$endRepeatDate = BAB_DateTime::fromIsoDateTime($arryRule['repeat_end_date']);
				$returnDescription.= ' ' . sprintf(workschedules_translate('until the %s'), $endRepeatDate->longFormat(false));
				
			}
			
			return $returnDescription;
			
		}else{
			return '';
		}
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function getDescription()
	{
		$return =  $this->getPeriodDescription();
		if($rrule = $this->rruleDescription()){
			$return.= ', '.$rrule;
		}
		return $return;
	}
}