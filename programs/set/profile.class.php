<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 *
 * @property ORM_StringField $name
 * @property ORM_BoolField $disabled
 * @property ORM_DatetimeField $modifiedOn
 * @property ORM_UserField $user
 * @property ORM_TimeField $week_duration
 *
 * @method workschedules_Profile newRecord()
 * @method workschedules_Profile[] select()
 * @method workschedules_Profile get()
 */
class workschedules_ProfileSet extends ORM_MySqlRecordSet
{
	public function __construct()
	{
		parent::__construct();

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('name'),
			ORM_BoolField('disabled'),
			ORM_DatetimeField('modifiedOn'),
			ORM_UserField('user'),
		    ORM_TimeField('week_duration')->setDescription('average week duration for the profile')
		);

	}


	/**
	 * @return array
	 */
	public function getOptions()
	{
		$res = $this->select($this->user->is(''))->orderAsc($this->name);

		$options = array('' => '');
		foreach($res as $profile)
		{
			$options[$profile->id] = $profile->name;
		}

		return $options;
	}
}

/**
 *
 * @property string $name
 * @property int $disabled
 * @property string $modifiedOn
 * @property int $user
 * @property string $week_duration
 */
class workschedules_Profile extends ORM_MySqlRecord
{
	private $ruleSet;


	public function isNamedProfile()
	{
		if (!isset($this->name))
		{
			return false;
		}

		return ('' !== $this->name);
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		if (!empty($this->name))
		{
			return $this->name;
		}

		$res = $this->selectUsers();
		if (1 === $res->count())
		{
			foreach($res as $userProfile)
			{
				return bab_getUserName($userProfile->user);
			}
		}

		return null;
	}


	/**
	 * Set the user of work schedules
	 * @return bool
	 */
	public function setUser($id_user)
	{
		if ($this->isNamedProfile())
		{
			throw new Exception(sprintf('This is a profile, do not set one user %s', $this->name));
		}

		if (empty($this->id))
		{
			throw new Exception('The profile must be saved');
		}

		$set = new workschedules_UserProfileSet();
		$set->delete($set->profile->is($this->id));

		$userProfile = $set->newRecord();

		$userProfile->profile = $this->id;
		$userProfile->user = $id_user;
		return $userProfile->save();
	}

	/**
	 * @return workschedules_UserProfile[] <workschedules_UserProfile>
	 */
	public function selectUsers()
	{
		$set = new workschedules_UserProfileSet();
		return $set->select($set->profile->is($this->id));
	}


	/**
	 * @return workschedules_RuleSet
	 */
	public function ruleSet($set = null)
	{
	    if (isset($set)) {
	        $this->ruleSet = $set;
	    }

	    if (!isset($this->ruleSet)) {
	        require_once dirname(__FILE__).'/rule.class.php';
	        $this->ruleSet = new workschedules_RuleSet();
	    }

	    return $this->ruleSet;
	}



	/**
	 * @return workschedules_Rule[] <workschedules_Rule>
	 */
	public function selectRules()
	{
		$set = $this->ruleSet();
		$res = $set->select($set->profile->is($this->id));
		$res->orderAsc($set->sortkey);

		return $res;
	}


	public function deleteRules()
	{
		$set = $this->ruleSet();
		$set->delete($set->profile->is($this->id));
	}


	public function delete()
	{
		$this->deleteRules();

		// unlink users
		$upSet = new workschedules_UserProfileSet();
		foreach($this->selectUsers() as $userProfile)
		{
			/*@var $userProfile workschedules_UserProfile */
			$upSet->delete($upSet->id->is($userProfile->id));

			workschedules_PeriodModified((int) $userProfile->user);
		}

		$set = $this->getParentSet();
		$set->delete($set->id->is($this->id));
	}


	/**
	 * Combine include and exclude
	 *
	 * @param BAB_DateTime $begin
	 * @param BAB_DateTime $end
	 *
	 * @return array
	 */
	public function getRulesPeriods(BAB_DateTime $begin, BAB_DateTime $end)
	{
		$notExclude = $this->getNotExcludePeriodes($begin, $end);
		if ($notExclude)
		{
			$exclude = $this->getExcludePeriodes($begin, $end);
		} else {
			$exclude = array();
		}

		$allArr = array();
		foreach($notExclude as $k => $v){
			$allArr[$k][] = $v;
		}
		foreach($exclude as $k => $v){
			$allArr[$k][] = $v;
		}

		ksort($allArr);
		$openboundaries = 0;
		$excludeboundaries = 0;
		$status = 0;
		$periodeBoundaries = array();

		foreach($allArr as $allboundery){

			$tempOpenboundaries = 0;
			$tempExcludeboundaries = 0;
			foreach($allboundery as $date){

				if($date['exclude']){
					$tempExcludeboundaries+= $date['status'];
				}else{
					$tempOpenboundaries+= $date['status'];
				}
			}

			$currentOpen = $openboundaries + $tempOpenboundaries;
			$currentExclude = $excludeboundaries + $tempExcludeboundaries;

			if($status == 0 && $currentExclude <= 0 && $currentOpen >0){
				$status = 1;
				$periodeBoundaries[$date['babDate']->getTimeStamp()] = $date['babDate'];
			}elseif($status == 1 && ( $currentExclude > 0 || $currentOpen <= 0) ){
				$status = 0;
				$periodeBoundaries[$date['babDate']->getTimeStamp()] = $date['babDate'];
			}

			$openboundaries+= $tempOpenboundaries;
			$excludeboundaries+= $excludeboundaries;
		}

		$periodes = array();
		$i = 0;
		$k = 0;
		foreach($periodeBoundaries as $date){
			if($k==0){
				$periodes[$i]['start'] = $date;
				$k++;
			}else{
				$periodes[$i]['end'] = $date;
				$i++;
				$k = 0;
			}
		}

		return $periodes;
	}


	/**
	 * @param BAB_DateTime $begin
	 * @param BAB_DateTime $end
	 *
	 * @return array
	 */
	protected function getExcludePeriodes(BAB_DateTime $begin, BAB_DateTime $end)
	{
		require_once dirname(__FILE__).'/../RRule.php';
		$ruleSet = $this->ruleSet();
		$rules = $ruleSet->select($ruleSet->profile->is($this->id)->_AND_($ruleSet->exclude->is(1)));

		$boundaries = array();
		foreach($rules as $rule){
			$startDate = BAB_DateTime::fromIsoDateTime($rule->begin);
			$endDate = BAB_DateTime::fromIsoDateTime($rule->end);
			$nbDay = BAB_DateTime::dateDiffIso($startDate->getIsoDate(), $endDate->getIsoDate());

			if($rule->rrule){
				$rrule = new workschedules_RRule(new workschedules_iCalDate($startDate->getICal(false)), $rule->rrule);
				while($startNext = $rrule->GetNext()){
					$startNext = BAB_DateTime::fromIsoDateTime($startNext->render());
					if ($startNext->getIsoDateTime() >= $end->getIsoDateTime())
					{
						break;
					}

					$endNext = $startNext->cloneDate();
					if($startNext->getIsoDateTime() >= $begin->getIsoDateTime()){
						if($nbDay>0){
							$endNext->add($nbDay);
						}
						$endNext->setTime($endDate->getHour(), $endDate->getMinute(), $endDate->getSecond());

						if($endNext->getIsoDateTime() > $end->getIsoDateTime()){
							$endNext = $end->cloneDate();
						}
						$boundaries[$startNext->getTimeStamp()][] = array('status' => '1', 'babDate' => $startNext);
						$boundaries[$endNext->getTimeStamp()][] = array('status' => '-1', 'babDate' => $endNext);
					}
				}
			}
			$boundaries[$startDate->getTimeStamp()][] = array('status' => '1', 'babDate' => $startDate);
			$boundaries[$endDate->getTimeStamp()][] = array('status' => '-1', 'babDate' => $endDate);

		}

		ksort($boundaries);
		$periodeBoundaries = array();
		$openboundaries = 0;
		foreach($boundaries as $boundary){
			$tempOpenboundaries = 0;
			foreach($boundary as $date){
				$tempOpenboundaries+= $date['status'];
			}

			if($tempOpenboundaries == 0){
			}else if ($openboundaries == 0 && $tempOpenboundaries > 0){
				$date['exclude'] = true;
				$periodeBoundaries[$date['babDate']->getTimeStamp()] = $date;
			}else if (($openboundaries + $tempOpenboundaries) <= 0){
				$date['exclude'] = true;
				$periodeBoundaries[$date['babDate']->getTimeStamp()] = $date;
			}
			$openboundaries+= $tempOpenboundaries;
		}

		return $periodeBoundaries;
	}


	/**
	 *
	 *
	 * @param BAB_DateTime $begin
	 * @param BAB_DateTime $end
	 *
	 * @return array
	 */
	protected function getNotExcludePeriodes(BAB_DateTime $begin, BAB_DateTime $end)
	{
		require_once dirname(__FILE__).'/../RRule.php';
		$ruleSet = $this->ruleSet();
		$rules = $ruleSet->select($ruleSet->profile->is($this->id)->_AND_($ruleSet->exclude->is(0)));

		$boundaries = array();
		foreach($rules as $rule) {


			$startDate = BAB_DateTime::fromIsoDateTime($rule->begin);
			$endDate = BAB_DateTime::fromIsoDateTime($rule->end);
			$nbDay = BAB_DateTime::dateDiffIso($startDate->getIsoDate(), $endDate->getIsoDate());

			if($rule->rrule){

				$rrule = new workschedules_RRule($startDate->getICal(false), $rule->rrule);

				while($startNext = $rrule->GetNext()){
					$startNext = BAB_DateTime::fromIsoDateTime($startNext->Render());
					$endNext = $startNext->cloneDate();
					if($nbDay>0){
						$endNext->add($nbDay);
					}
					$endNext->setTime($endDate->getHour(), $endDate->getMinute(), $endDate->getSecond());

					if($endNext->getIsoDateTime() >= $begin->getIsoDateTime() && $startNext->getIsoDateTime() <= $end->getIsoDateTime()){

						if($endNext->getIsoDateTime() > $end->getIsoDateTime()){
							$endNext = $end->cloneDate();
						}
						$boundaries[$startNext->getTimeStamp()][] = array('status' => '1', 'babDate' => $startNext);
						$boundaries[$endNext->getTimeStamp()][] = array('status' => '-1', 'babDate' => $endNext);
					}

					if ($end->getIsoDateTime() < $startNext->getIsoDateTime())
					{
						break;
					}
				}
			}



			if ($endDate->getIsoDateTime() >= $begin->getIsoDateTime() && $startDate->getIsoDateTime() <= $end->getIsoDateTime()) {
				$boundaries[$startDate->getTimeStamp()][] = array('status' => '1', 'babDate' => $startDate);
				$boundaries[$endDate->getTimeStamp()][] = array('status' => '-1', 'babDate' => $endDate);
			}
		}

		ksort($boundaries);
		$periodeBoundaries = array();
		$openboundaries = 0;
		foreach($boundaries as $boundary){
			$tempOpenboundaries = 0;
			foreach($boundary as $date){
				$tempOpenboundaries+= $date['status'];
			}

			if($tempOpenboundaries == 0){
			}else if ($openboundaries == 0 && $tempOpenboundaries > 0){
				$date['exclude'] = false;
				$periodeBoundaries[$date['babDate']->getTimeStamp()] = $date;
			}else if (($openboundaries + $tempOpenboundaries) <= 0){
				$date['exclude'] = false;
				$periodeBoundaries[$date['babDate']->getTimeStamp()] = $date;
			}
			$openboundaries+= $tempOpenboundaries;
		}

		return $periodeBoundaries;
	}
}