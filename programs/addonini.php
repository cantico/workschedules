;<?php /*

[general]
name							="workschedules"
version							="1.8"
addon_type						="EXTENSION"
encoding						="UTF-8"
mysql_character_set_database	="latin1,utf8"
description						="Work schedules for calendar and vacations"
description.fr					="Rythmes de travail pour les agendas et les congés"
delete							=1
ov_version						="8.1.95"
php_version						="5.1.0"
addon_access_control			="0"
author							="Cantico"
icon							="icon.png"
tags                            ="extension,calendar"

[addons]

widgets							="1.0.34"
LibOrm							="0.8.7"
LibTranslate					=">=1.12.0rc3.01"

;*/