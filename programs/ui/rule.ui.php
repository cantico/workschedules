<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';


bab_Widgets()->includePhpClass('Widget_Form');

class workschedules_RuleEditor extends Widget_Form
{
	public function __construct(workschedules_Rule $rule = null, $id = null, Widget_Layout $layout = null)
	{
		$W = bab_Widgets();
		
		if (null === $layout)
		{
			$layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');
		}
		
		parent::__construct($id, $layout);
		
		$this->setName('rule');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-bordered');
		
		$this->setHiddenValue('tg', bab_rp('tg'));
		
		$this->layout = $layout;
		
		$this->addFields();
		
		$this->addItem($buttons = $W->HBoxLayout()->setHorizontalSpacing(1,'em'));
		
		
		$buttons->addItem(
			$W->SubmitButton()
				->setAction(workschedules_Controller()->Rule()->save())
				->setSuccessAction(workschedules_Controller()->Rule()->edit())
				->setFailedAction(workschedules_Controller()->Rule()->edit())
				->setLabel(workschedules_translate('Save'))
		);
		
		if (isset($rule))
		{
			$buttons->addItem(
				$W->SubmitButton()
					->setAction(workschedules_Controller()->Rule()->delete())
					->setSuccessAction(workschedules_Controller()->Profile()->display($rule->profile))
					->setFailedAction(workschedules_Controller()->Rule()->edit())
					->setLabel(workschedules_translate('Delete'))
					->setConfirmationMessage(workschedules_translate('Do you really want to delete this rule?'))
			);
		}
		
		$this->layout->addItem(
				$hidden = $W->Hidden()->setName('id')
		);
		
		
		if($rule){
			$ruleArray = $rule->getValues();
			require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
			
			$beginBabDate = BAB_DateTime::fromIsoDateTime($rule->begin);
			$ruleArray['period'] = $beginBabDate->getIsoDate();
			$ruleArray['timebegin'] = $beginBabDate->getIsoTime();
			
			$endBabDate = BAB_DateTime::fromIsoDateTime($rule->end);
			$ruleArray['timeend'] = $endBabDate->getIsoTime();
			
			$this->setValues($ruleArray, array('rule'));
			$this->setValues(workschedules_getRRuleForm($rule->rrule), array('rule'));
			
			$hidden->setValue($rule->id);
		}
		
		if(isset($_POST['rule'])){
			$this->setValues(array('rule' => $_POST['rule']));
		}
	}
	
	
	public function setProfile($idProfile)
	{
		$W = bab_Widgets();
		
		$this->layout->addItem(
			$W->Hidden()->setValue($idProfile)->setName('profile')
		);
	}

	
	protected function addFields()
	{
		$W = bab_Widgets();
		
		
		
		$this->addItem($this->exclude());
		
		$this->addItem(
			$W->VBoxItems(
				$W->Title(workschedules_translate('Worked day'),4),
				$this->period()
			)
		);
		
		$repeatFrame = $this->repeatFrame();
		$this->addItem(
			$W->VBoxItems(
				$this->repeat($repeatFrame),
				$repeatFrame
			)
		);
	}
	
	protected function daily(){
		$W = bab_Widgets();
		
		return $W->VBoxItems(
			$W->HBoxItems(
				$W->Label(workschedules_translate('All those')),
				$W->LineEdit()->setMaxSize(4)->setSize(4)->setName('repeat_n_d'),
				$W->Label(workschedules_translate('days'))
			)->setHorizontalSpacing(.25, 'em')->setVerticalAlign('middle')
		);
	}
	
	protected function weekly(){
		$W = bab_Widgets();
		
		return $W->VBoxItems(
			$W->HBoxItems(
				$W->Label(workschedules_translate('All those (week)')),
				$W->LineEdit()->setMaxSize(4)->setSize(4)->setName('repeat_n_w'),
				$W->Label(workschedules_translate('weeks'))
			)->setHorizontalSpacing(.25, 'em')->setVerticalAlign('middle'),
			$W->HBoxItems(
				$W->HBoxItems(
					$tmpLbl = $W->Label(workschedules_translate('Sun')),
					$W->CheckBox()->setName(array('repeat_wd','SU'))->setCheckedValue('SU')->setAssociatedLabel($tmpLbl)
				)->setHorizontalSpacing(.25, 'em')->setVerticalAlign('middle'),
				$W->HBoxItems(
					$tmpLbl = $W->Label(workschedules_translate('Mon')),
					$W->CheckBox()->setName(array('repeat_wd','MO'))->setCheckedValue('MO')->setAssociatedLabel($tmpLbl)
				)->setHorizontalSpacing(.25, 'em')->setVerticalAlign('middle'),
				$W->HBoxItems(
					$tmpLbl = $W->Label(workschedules_translate('Tue')),
					$W->CheckBox()->setName(array('repeat_wd','TU'))->setCheckedValue('TU')->setAssociatedLabel($tmpLbl)
				)->setHorizontalSpacing(.25, 'em')->setVerticalAlign('middle'),
				$W->HBoxItems(
					$tmpLbl = $W->Label(workschedules_translate('Wen')),
					$W->CheckBox()->setName(array('repeat_wd','WE'))->setCheckedValue('WE')->setAssociatedLabel($tmpLbl)
				)->setHorizontalSpacing(.25, 'em')->setVerticalAlign('middle'),
				$W->HBoxItems(
					$tmpLbl = $W->Label(workschedules_translate('The')),
					$W->CheckBox()->setName(array('repeat_wd','TH'))->setCheckedValue('TH')->setAssociatedLabel($tmpLbl)
				)->setHorizontalSpacing(.25, 'em')->setVerticalAlign('middle'),
				$W->HBoxItems(
					$tmpLbl = $W->Label(workschedules_translate('Fri')),
					$W->CheckBox()->setName(array('repeat_wd','FR'))->setCheckedValue('FR')->setAssociatedLabel($tmpLbl)
				)->setHorizontalSpacing(.25, 'em')->setVerticalAlign('middle'),
				$W->HBoxItems(
					$tmpLbl = $W->Label(workschedules_translate('Sat')),
					$W->CheckBox()->setName(array('repeat_wd','SA'))->setCheckedValue('SA')->setAssociatedLabel($tmpLbl)
				)->setHorizontalSpacing(.25, 'em')->setVerticalAlign('middle')
			)->setHorizontalSpacing(.5, 'em')
		);
	}
	
	protected function monthly(){
		$W = bab_Widgets();
		
		return $W->VBoxItems(
			$W->HBoxItems(
				$W->Label(workschedules_translate('All those')),
				$W->LineEdit()->setMaxSize(4)->setSize(4)->setName('repeat_n_m'),
				$W->Label(workschedules_translate('months'))
			)->setHorizontalSpacing(.25, 'em')->setVerticalAlign('middle')
		);
	}
	
	protected function yearly(){
		$W = bab_Widgets();
		
		return $W->VBoxItems(
			$W->HBoxItems(
				$W->Label(workschedules_translate('All those')),
				$W->LineEdit()->setMaxSize(4)->setSize(4)->setName('repeat_n_y'),
				$W->Label(workschedules_translate('years'))
			)->setHorizontalSpacing(.25, 'em')->setVerticalAlign('middle')
		);
	}
	
	
	protected function repeat(Widget_Displayable_Interface $widget)
	{
		$W = bab_Widgets();
		return $W->labelledWidget(
				workschedules_translate('Repeat'),
				$W->CheckBox()->setCheckedValue('1')->setAssociatedDisplayable($widget, array('1')),
				__FUNCTION__
		);
	}
	
	protected function repeatFrame()
	{
		$W = bab_Widgets();
		
		$daily = $this->daily();
		$weekly = $this->weekly();
		$monthly = $this->monthly();
		$yearly = $this->yearly();
		
		$repeatEndDate = $W->DatePicker()->setMinDate($this->dateStart)->setName('repeat_end_date')->setValue(date('d-m-Y'));
		
		return $W->VBoxItems(
			$W->HBoxItems(
				$W->VBoxItems(
					$W->RadioSet()->setName('repeat_frequency')->setValue('daily')
						->addOption('daily', workschedules_translate('Daily'))
						->setAssociatedDisplayable($daily, array('daily'))
						->addOption('weekly', workschedules_translate('Weekly'))
						->setAssociatedDisplayable($weekly, array('weekly'))
						->addOption('monthly', workschedules_translate('Monthly'))
						->setAssociatedDisplayable($monthly, array('monthly'))
						->addOption('yearly', workschedules_translate('Yearly'))
						->setAssociatedDisplayable($yearly, array('yearly'))
				),
				$W->VBoxItems(
					$W->VBoxItems(
						$daily,
						$weekly,
						$monthly,
						$yearly
					),
					$W->VBoxItems(
						$W->labelledWidget(
							workschedules_translate('Use an end date repeat periode'),
							$W->CheckBox()->setCheckedValue('1')->setAssociatedDisplayable($repeatEndDate, array('1')),
							'repeat_end_cb'
						),
						$repeatEndDate
					)->setVerticalSpacing(.25, 'em')
				)->setVerticalSpacing(1, 'em')
			)->setVerticalAlign('middle')->setHorizontalSpacing(3, 'em')
		)->setId('repeat-form');
		
	}
	
	
	protected function exclude()
	{
		$W = bab_Widgets();
		
		return $W->labelledWidget(
			workschedules_translate('Use this rule to exclude worked days'),
			$W->CheckBox()->setCheckedValue('1'),
			__FUNCTION__
		);
	}
	
	
	protected function period()
	{
		$W = bab_Widgets();
		
		
		$start = $W->TimeEdit()->setName('timebegin');
		$end = $W->TimeEdit()->setName('timeend');
		
		return $W->VBoxItems(
			$W->HBoxItems(
				//TRANSLATORS: prefix for date field
				$dateLabel = $W->Label(workschedules_translate('The day')),
				$this->dateStart = $W->DatePicker()
					->setName(__FUNCTION__)
					->setValue(date('Y-m-d'))
					->setSelectableMonth()
					->setSelectableYear()
					->setAssociatedLabel($dateLabel),
				//TRANSLATORS: prefix for hour field
				$fromLabel = $W->Label(workschedules_translate('From')),
				$start->setAssociatedLabel($fromLabel)->setValue('08:00:00'),
				//TRANSLATORS: prefix for hour field
				$toLabel = $W->Label(workschedules_translate('To')),
				$end->setAssociatedLabel($toLabel)->setValue('18:00:00')
			)->setVerticalAlign('middle')->setHorizontalSpacing(.5, 'em')
		)->setVerticalSpacing(.5,'em');
	}
}






















