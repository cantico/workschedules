<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';


bab_Widgets()->includePhpClass('Widget_Form');

class workschedules_AdminEditor extends Widget_Form
{
	public function __construct($id = null, Widget_Layout $layout = null)
	{
		$W = bab_Widgets();
		
		if (null === $layout)
		{
			$layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');
		}
		
		parent::__construct($id, $layout);
		
		$this->setName('options');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-bordered');
		
		$this->setHiddenValue('tg', bab_rp('tg'));
		
		$this->addFields();
		
		$this->addItem(
			$W->SubmitButton()
				->setAction(workschedules_Controller()->Admin()->save())
				->setSuccessAction(workschedules_Controller()->Admin()->edit())
				->setFailedAction(workschedules_Controller()->Admin()->edit())
				->setLabel(workschedules_translate('Save'))
		);
		
		$this->loadValues();
	}
	
	
	protected function loadValues()
	{
		require_once $GLOBALS['babInstallPath'].'admin/acl.php';
		$this->setValue(array('options', 'manager'), aclGetRightsString('workschedules_manager_groups', 1));
	}
	
	
	protected function addFields()
	{
		$W = bab_Widgets();
		$this->addItem($W->Acl()->disableGroup(0)/*->disableGroup(2)*/->setName('manager')->setTitle(workschedules_translate('Work schedules managers')));
	}
	
	
	
}