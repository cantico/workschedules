<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';


bab_Widgets()->includePhpClass('Widget_BabTableModelView');
bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('Widget_Frame');


class workschedules_ProfileTableView extends Widget_BabTableModelView
{
	public function addDefaultColumns(workschedules_ProfileSet $set)
	{
		$this->addClass(Func_Icons::ICON_LEFT_16);
		
		$this->addColumn(widget_TableModelViewColumn('_view', '')->addClass('widget-column-thin'));
		$this->addColumn(widget_TableModelViewColumn($set->name, workschedules_translate('Name')));
		$this->addColumn(widget_TableModelViewColumn('_rules', workschedules_translate('Rules')));
		$this->addColumn(widget_TableModelViewColumn($set->modifiedOn, workschedules_translate('Modified On')));
	}
	
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(workschedules_Profile $record, $fieldPath)
	{
		$W = bab_Widgets();
		
		if ('_view' === $fieldPath)
		{
			return $W->Link($W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT), workschedules_Controller()->Profile()->edit($record->id));
		}
		
		if ('_rules' === $fieldPath)
		{
			return $W->Link($W->Icon(workschedules_translate('View the rules'), Func_Icons::ACTIONS_DOCUMENT_PROPERTIES), workschedules_Controller()->Profile()->display($record->id));
		}
		
		return parent::computeCellContent($record, $fieldPath);
	}
}


class workschedules_UserProfileTableView extends Widget_BabTableModelView
{
	public function addDefaultColumns(workschedules_UserSet $set)
	{
		$this->addClass(Func_Icons::ICON_LEFT_16);
		
		$this->addColumn(widget_TableModelViewColumn('_remove', '')->addClass('widget-column-thin'));
		$this->addColumn(widget_TableModelViewColumn($set->user, workschedules_translate('User name'))->addClass('widget-strong'));
		$this->addColumn(widget_TableModelViewColumn('_pprofile', workschedules_translate('Personnal profile')));
		$this->addColumn(widget_TableModelViewColumn('_sprofile', workschedules_translate('Share profile')));
	}
	
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(workschedules_User $record, $fieldPath)
	{
		$W = bab_Widgets();
		$set = new workschedules_UserProfileSet();
		$set->profile();
		
		if ('user' === $fieldPath) {
		    $username = bab_getUserName($record->user);
		    
		    if ($record->username_copy !== $username) {
		        $record->username_copy = $username;
		        $record->save();
		    }
		}
		
		
		
		
		
		if ('_remove' === $fieldPath)
		{
			return $W->Link(
				$W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE),
				workschedules_Controller()->Profile()->removeUser($record->user)
			)->setConfirmationMessage(workschedules_translate('This will remove this user from the work schedule profile definition ?'));
		}
		
		
		if ('_pprofile' === $fieldPath)
		{
			$pprofiles = $set->select($set->user->is($record->user)->_AND_($set->profile->user->is($record->user)));
			
			$pprofileLayout = $W->FlowLayout()->setHorizontalSpacing(.25,'em')->setVerticalSpacing(.25,'em');
			
			foreach($pprofiles as $pprofile){
				$pprofileLayout->addItem(
					$W->Link(
						$W->Icon($pprofile->profile->name, Func_Icons::ACTIONS_DOCUMENT_PROPERTIES),
						workschedules_Controller()->Profile()->display($pprofile->profile->id)
					)
				);
				$pprofileLayout->addItem($W->Label('-'));
			}
			
			return $pprofileLayout->addItem(
				$W->Link(
					$W->Icon(workschedules_translate('Add a personnal profile'), Func_Icons::ACTIONS_DOCUMENT_NEW),
					workschedules_Controller()->Profile()->edit(null, $record->user)
				)
			);
		}
		
		
		if ('_sprofile' === $fieldPath)
		{
			$sprofiles = $set->select($set->user->is($record->user)->_AND_($set->profile->user->is(0)));
			
			$sprofileLayout = $W->FlowLayout()->setHorizontalSpacing(.25,'em')->setVerticalSpacing(.25,'em');
			
			foreach($sprofiles as $sprofile){
				$sprofileLayout->addItem(
					$W->Link(
						$W->Icon($sprofile->profile->name, Func_Icons::ACTIONS_DOCUMENT_PROPERTIES),
						workschedules_Controller()->Profile()->addUserProfile($record->user, $sprofile->id)
					)
				);
				$sprofileLayout->addItem($W->Label('-'));
			}
			
			return $sprofileLayout->addItem(
				$W->Link(
					$W->Icon(workschedules_translate('Add a share profile'), Func_Icons::ACTIONS_DOCUMENT_NEW),
					workschedules_Controller()->Profile()->addUserProfile($record->user)
				)
			);
		}
		
		return parent::computeCellContent($record, $fieldPath);
	}
}

class workschedules_UserProfileUserTableView extends Widget_BabTableModelView
{
	public function addDefaultColumns(workschedules_UserProfileSet $set)
	{
		$this->addClass(Func_Icons::ICON_LEFT_16);
		
		$this->addColumn(widget_TableModelViewColumn($set->profile->name, workschedules_translate('Profile'))->addClass('widget-strong'));
		$this->addColumn(widget_TableModelViewColumn($set->from, workschedules_translate('from_date')));
		$this->addColumn(widget_TableModelViewColumn($set->to, workschedules_translate('to_date')));
		$this->addColumn(widget_TableModelViewColumn('_type_', workschedules_translate('Type')));
	}
	
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(workschedules_UserProfile $record, $fieldPath)
	{
		$W = bab_Widgets();
		$set = new workschedules_UserProfileSet();
		$set->profile();
		
		if ('_type_' === $fieldPath)
		{
			if($record->profile->user == 0){
				$typetxt = workschedules_translate('Shared profile');
			}else{
				$typetxt = workschedules_translate('Personnal profile');
			}
			return $W->Label($typetxt);
		}
		
		
		if ('_pprofile' === $fieldPath)
		{
			$pprofiles = $set->select($set->user->is($record->user)->_AND_($set->profile->user->is($record->user)));
			
			$pprofileLayout = $W->FlowLayout()->setHorizontalSpacing(.25,'em')->setVerticalSpacing(.25,'em');
			
			foreach($pprofiles as $pprofile){
				$pprofileLayout->addItem(
					$W->Link(
						$W->Icon($pprofile->profile->name, Func_Icons::ACTIONS_DOCUMENT_PROPERTIES),
						workschedules_Controller()->Profile()->display($pprofile->profile->id)
					)
				);
				$pprofileLayout->addItem($W->Label('-'));
			}
			
			return $pprofileLayout->addItem(
				$W->Link(
					$W->Icon(workschedules_translate('Add a personnal profile'), Func_Icons::ACTIONS_DOCUMENT_NEW),
					workschedules_Controller()->Profile()->edit(null, $record->user)
				)
			);
		}
		
		
		if ('_sprofile' === $fieldPath)
		{
			$sprofiles = $set->select($set->user->is($record->user)->_AND_($set->profile->user->is(0)));
			
			$sprofileLayout = $W->FlowLayout()->setHorizontalSpacing(.25,'em')->setVerticalSpacing(.25,'em');
			
			foreach($sprofiles as $sprofile){
				$sprofileLayout->addItem(
					$W->Link(
						$W->Icon($sprofile->profile->name, Func_Icons::ACTIONS_DOCUMENT_PROPERTIES),
						workschedules_Controller()->Profile()->addUserProfile($record->user, $sprofile->id)
					)
				);
				$sprofileLayout->addItem($W->Label('-'));
			}
			
			return $sprofileLayout->addItem(
				$W->Link(
					$W->Icon(workschedules_translate('Add a share profile'), Func_Icons::ACTIONS_DOCUMENT_NEW),
					workschedules_Controller()->Profile()->addUserProfile($record->user)
				)
			);
		}
		
		return parent::computeCellContent($record, $fieldPath);
	}
}


class workschedules_RuleTableView extends Widget_BabTableModelView
{
	public function addDefaultColumns(workschedules_RuleSet $set)
	{
		$this->addClass(Func_Icons::ICON_LEFT_16);
		
		$this->addColumn(widget_TableModelViewColumn('_edit', '')->addClass('widget-column-thin'));
		$this->addColumn(widget_TableModelViewColumn($set->exclude, workschedules_translate('Exclude')));
		$this->addColumn(widget_TableModelViewColumn('_description', workschedules_translate('Description')));
	}
	
	
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(workschedules_Rule $record, $fieldPath)
	{
		$W = bab_Widgets();
		
		if ('_edit' === $fieldPath)
		{
			return $W->Link($W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT), workschedules_Controller()->Rule()->edit($record->id));
		}
		
		if ('_description' === $fieldPath)
		{
			return $W->Label($record->getDescription());
		}
		
		if($fieldPath == 'exclude')
		{
			$label = workschedules_translate('No');

			if($record->exclude){
				$label = workschedules_translate('Yes');
			}
			
			return $W->Label($label);
		}
		
		return parent::computeCellContent($record, $fieldPath);
	}
}



class workschedules_UserSelector extends Widget_Form
{
	public function __construct($id = null, Widget_Layout $layout = null)
	{
		$W = bab_Widgets();
	
		if (null === $layout)
		{
			$layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');
		}
	
		parent::__construct($id, $layout);
	
		$this->setName('user');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-bordered');
	
		$this->setHiddenValue('tg', bab_rp('tg'));
		$failedAction = workschedules_Controller()->Profile()->chooseUser();
		
		$this->addFields();
		
		
		$buttons = $W->FlowItems()->setHorizontalSpacing(1,'em');
		$this->addItem($buttons);
		
		
		$buttons->addItem(
				$W->SubmitButton()
				->setAction(workschedules_Controller()->Profile()->cancel())
				->setLabel(workschedules_translate('Cancel'))
		);
		
		
		//creation of a user profile
		$buttons->addItem(
			$W->SubmitButton()
				->setAction(workschedules_Controller()->Profile()->userSave())
				->setSuccessAction(workschedules_Controller()->Profile()->displayUsersList())
				->setFailedAction($failedAction)
				->setLabel(workschedules_translate('Next'))
		);
	}
	
	
	
	protected function addFields()
	{
		$W = bab_Widgets();
		
		$user = $W->Frame()->addItem($this->user());
		
		$this->addItem($W->Items($user));
		
	}
	
	
	protected function user()
	{
		$W = bab_Widgets();
		
		return $W->labelledWidget(
				workschedules_translate('User'),
				$W->UserPicker()->setMandatory(true, workschedules_translate('The user is mandatory')),
				__FUNCTION__
		);
	}
}



class workschedules_ProfileEditor extends Widget_Form
{
	public function __construct(workschedules_Profile $profile = null, $user = null, $id = null, Widget_Layout $layout = null)
	{
		$W = bab_Widgets();
	
		if (null === $layout)
		{
			$layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');
		}
	
		parent::__construct($id, $layout);
		
		$this->profile = $profile;
		$this->user = $user;
	
		$this->setName('profile');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-bordered');
	
		$this->setHiddenValue('tg', bab_rp('tg'));
		$this->setHiddenValue('profile[user]', $user);
		
		if (isset($profile))
		{
			$this->setHiddenValue('profile[id]', $profile->id);
		}
	
		
		if (isset($profile))
		{
			$values = $profile->getValues();
		
			$this->setValues(array('profile' => $values));
			
			
			$failedAction = workschedules_Controller()->Profile()->edit($profile->id, $this->user);
			
		} else {
			$failedAction = workschedules_Controller()->Profile()->edit(null, $this->user);
		}
		
		
		$this->addFields();
		
		
		$buttons = $W->FlowItems()->setHorizontalSpacing(1,'em');
		$this->addItem($buttons);
		
		
		$buttons->addItem(
				$W->SubmitButton()
				->setAction(workschedules_Controller()->Profile()->cancel())
				->setLabel(workschedules_translate('Cancel'))
		);
		
		if($user){
			if (isset($profile))
			{
				$upset = new workschedules_UserProfileSet();
				$up = $upset->get($upset->user->is($user)->_AND_($upset->profile->is($profile->id)));
				$this->setValues(array('profile' => array('from' => $up->from, 'to' => $up->to)));
			}
			
			$SuccessAction = workschedules_Controller()->Profile()->displayUsersList();
		}else{
			$SuccessAction = workschedules_Controller()->Profile()->displayList();
		}
		
		if (!isset($profile))
		{
			// creation of a user profile or named profile
			$buttons->addItem(
				$W->SubmitButton()
					->setAction(workschedules_Controller()->Profile()->save())
					->setSuccessAction($SuccessAction)
					->setFailedAction($failedAction)
					->setLabel(workschedules_translate('Next'))
			);
		
		} else{
			
			// modification of a named profile
			
			$buttons->addItem(
					$W->SubmitButton()
					->setAction(workschedules_Controller()->Profile()->save())
					->setSuccessAction($SuccessAction)
					->setFailedAction($failedAction)
					->setLabel(workschedules_translate('Save'))
			);
			
			$buttons->addItem(
				$W->SubmitButton()
					->setAction(workschedules_Controller()->Profile()->delete())
					->setSuccessAction($SuccessAction)
					->setFailedAction($failedAction)
					->setLabel(workschedules_translate('Delete'))
					->setConfirmationMessage(workschedules_translate('Do you really want to delete this profile?'))
			);
		}
		
		if(isset($_POST['profile'])){
			$this->setValues(array('profile' => $_POST['profile']));
		}
	}
	
	
	
	protected function addFields()
	{
		$W = bab_Widgets();
		
		$name = $W->Frame()->addItem($this->name());
		
		$this->addItem($W->Items($name));
		if($this->user){
			$this->addItem($this->periode());
		}
		
		$this->addItem($this->week_duration());
	}
	


	protected function periode()
	{
		$W = bab_Widgets();
	
		return $W->labelledWidget(
				workschedules_translate('Validity periode'),
				$W->PeriodPicker(),
				''
		);
	}
	
	
	protected function name()
	{
		$W = bab_Widgets();
		
		return $W->labelledWidget(
				workschedules_translate('Profile name'),
				$W->LineEdit()->setSize(70)->setMandatory(true, workschedules_translate('The profile name is mandatory')),
				__FUNCTION__
		);
	}
	
	
	protected function week_duration()
	{
	    $W = bab_Widgets();
	
	    return $W->labelledWidget(
	        workschedules_translate('Average week duration'),
	        $W->LineEdit()->setSize(12)->setMaxSize(5),
	        __FUNCTION__,
	        workschedules_translate('Format HH:MM')
	    );
	}
}





class workschedules_UserProfileEditor extends Widget_Form
{

	
	public function __construct(workschedules_User $user, $uprofile = null,  $success_action = null, $failed_action = null)
	{
		
		$W = bab_Widgets();
		$layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');
	
		parent::__construct(null, $layout);
	
		$this->setName('userprofile');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-bordered');
		$this->colon();
	
		$this->setHiddenValue('tg', bab_rp('tg'));
		$this->setHiddenValue('userprofile[user]', $user->user);
		$this->setHiddenValue('userprofile[id]', $uprofile);

		$userName = bab_getUserName($user->user);
		$title = sprintf(workschedules_translate('Link a share profile to user %s'), $userName);
			
		$this->addItem($W->Title($title), 3);
		
		$this->addFields();
		
		$buttons = $W->FlowItems()->setHorizontalSpacing(1,'em');
		$this->addItem($buttons);
		
		if (isset($uprofile))
		{
			$uprofileSet = new workschedules_UserProfileSet();
			$userProfile = $uprofileSet->get($uprofile);
			$values = $userProfile->getValues();
		
			$this->setValues(array('userprofile' => $values));				
		}
	
		if (!isset($success_action))
		{
			$success_action = workschedules_Controller()->Profile()->displayUsersList();
		}
		
		if (!isset($failed_action))
		{
			$failed_action = workschedules_Controller()->Profile()->addUserProfile($user->user, $uprofile);
		}
	
		$buttons->addItem(
			$W->SubmitButton()->validate()
				->setAction(workschedules_Controller()->Profile()->saveUserProfile())
				->setFailedAction($failed_action)
				->setSuccessAction($success_action)
				->setLabel(workschedules_translate('Save'))
		);
		
		if (isset($uprofile))
		{
			$buttons->addItem(
				$W->SubmitButton()
					->setAction(workschedules_Controller()->Profile()->deleteUserProfile())
					->setSuccessAction($success_action)
					->setFailedAction($failed_action)
					->setLabel(workschedules_translate('Delete'))
					->setConfirmationMessage(workschedules_translate('Do you really want to unlink this profile ?'))
			);
		}
	}
	

	
	
	
	protected function addFields()
	{
		$this->addItem($this->profile_option());
		$this->addItem($this->periode());
	}
	
	protected function periode()
	{
		$W = bab_Widgets();
		
		return $W->labelledWidget(
			workschedules_translate('Validity periode'),
			$W->PeriodPicker(),
			''
		);
	}
	
	protected function profile_option()
	{
		$W = bab_Widgets();
	
		$set = new workschedules_ProfileSet;
		$select = $W->Select()->setOptions($set->getOptions());
	
		return $W->labelledWidget(
				workschedules_translate('Shared profile'),
				$select->setMandatory(true, workschedules_translate('The profile is mandatory')),
				'profile'
		);
	
	}

}




/**
 * Profile full frame
 *
 */
class workschedules_ProfileFullFrame extends Widget_Frame
{
	/**
	 * 
	 * @var workschedules_Profile
	 */
	protected $profile;
	
	
	public function __construct(workschedules_Profile $profile)
	{
		$W = bab_Widgets();
		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));
		
		$this->profile = $profile;
		
		$this->addItem($W->Title($profile->getName()));
		
		$this->addItem($this->rules());
	}
	
	
	public function rules()
	{
		$W = bab_Widgets();
		$res = $this->profile->selectRules();
		
		$list = new workschedules_RuleTableView();
		$list->addDefaultColumns($res->getSet());
		$list->setDataSource($res);
		

		if(!$this->profile->user){
			$editLink = null;
			$confirmMessage = workschedules_translate('This will remove this global profile. If users is link to it the deletion will not be completed.');
		}else{
			$editLink = $W->Link(
				$W->Icon(
					workschedules_translate('Edit this profile'),
					Func_Icons::ACTIONS_DOCUMENT_EDIT
				),
				workschedules_Controller()->Profile()->edit($this->profile->id, $this->profile->user)
			);
			$confirmMessage = workschedules_translate('This will remove this profile and all this rules, proceed ?');
		}
		
		$action = $W->HBoxItems(
			$W->Link(
				$W->Icon(workschedules_translate('Add a rule'), Func_Icons::ACTIONS_LIST_ADD),
				workschedules_Controller()->Rule()->edit(null, $this->profile->id)
			),
			$editLink,
			$W->Link(
				$W->Icon(workschedules_translate('Remove this profile'), Func_Icons::ACTIONS_EDIT_DELETE),
				workschedules_Controller()->Profile()->delete($this->profile->id)
			)->setConfirmationMessage($confirmMessage)
		)->addClass(Func_Icons::ICON_LEFT_16);
		

		
		return $W->VBoxItems($action, $list);
	}
	
}