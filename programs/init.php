<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
include "base.php";


function workschedules_onDeleteAddon()
{
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	include_once $GLOBALS['babInstallPath']."utilit/eventincl.php";

	$functionalities = new bab_functionalities();
	$functionalities->unregister('WorkingHours/Workschedules');

	bab_removeEventListener('bab_eventBeforeSiteMapCreated', 'workschedules_onSiteMapItems', 'addons/workschedules/init.php');

	return true;
}


function workschedules_upgrade($version_base, $version_ini)
{
	include_once $GLOBALS['babInstallPath']."utilit/eventincl.php";
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
	require_once $GLOBALS['babInstallPath'].'admin/acl.php';

	require_once dirname(__FILE__).'/functions.php';
	workschedules_loadOrm();

	require_once dirname(__FILE__).'/set/profile.class.php';
	require_once dirname(__FILE__).'/set/user.class.php';
	require_once dirname(__FILE__).'/set/userprofile.class.php';
	require_once dirname(__FILE__).'/set/rule.class.php';

	$functionalities = new bab_functionalities();
	$functionalities->register('WorkingHours/Workschedules', dirname(__FILE__).'/workinghours.class.php');

	workschedules_loadOrm();
	$synchronize = new bab_synchronizeSql();
	$synchronize->addOrmSet(new workschedules_ProfileSet);
	$synchronize->addOrmSet(new workschedules_UserSet);
	$synchronize->addOrmSet(new workschedules_UserProfileSet);
	$synchronize->addOrmSet(new workschedules_RuleSet);
	$synchronize->updateDatabase();

	aclCreateTable('workschedules_manager_groups');

	bab_removeAddonEventListeners('workschedules');
	$addon = bab_getAddonInfosInstance('workschedules');
	if (method_exists($addon, 'addEventListener')) {
		$addon->addEventListener('bab_eventBeforeSiteMapCreated', 'workschedules_onSiteMapItems', 'init.php');
	}else{
		bab_addEventListener('bab_eventBeforeSiteMapCreated', 'workschedules_onSiteMapItems', 'addons/workschedules/init.php', 'workschedules');
	}

	return true;
}




/**
 * Sitemap creation
 * @param bab_eventBeforeSiteMapCreated $event
 * @return mixed
 */
function workschedules_onSiteMapItems(bab_eventBeforeSiteMapCreated $event) {

	require_once dirname(__FILE__).'/functions.php';

	bab_functionality::includefile('Icons');


	if (bab_isAccessValid('workschedules_manager_groups', 1)) {
		$link = $event->createItem('workschedulesManager');
		$link->setLabel(workschedules_translate('Work schedules'));
		$link->setLink('?tg=addon/workschedules/main&idx=profile.displaylist');
		$link->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons'));
		$link->addIconClassname(Func_Icons::APPS_PREFERENCES_CALENDAR);

		$event->addFunction($link);
	}


	if (bab_isUserAdministrator()) {

		$link = $event->createItem('workschedulesAdmin');
		$link->setLabel(workschedules_translate('Work schedules'));
		$link->setLink('?tg=addon/workschedules/main&idx=admin.edit');
		$link->setPosition(array('root', 'DGAll', 'babAdmin', 'babAdminSectionAddons'));
		$link->addIconClassname(Func_Icons::APPS_PREFERENCES_CALENDAR);

		$event->addFunction($link);
	}

}
