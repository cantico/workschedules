<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';



/**
 * Translate
 * 
 * @param string $str
 * @param string $str_plurals
 * @param int $number
 * @return string|null
 */
function workschedules_translate($str, $str_plurals = null, $number = null)
{
	if ($translate = bab_functionality::get('Translate/Gettext'))
	{
		/* @var $translate Func_Translate_Gettext */
		$translate->setAddonName('workschedules');
		
		return $translate->translate($str, $str_plurals, $number);
	}
	
	return $str;
}


/**
 * Initialize mysql ORM backend.
 */
function workschedules_loadOrm()
{
	if (!class_exists('ORM_MySqlRecordSet'))
	{
		$Orm = bab_functionality::get('LibOrm');
		/*@var $Orm Func_LibOrm */
		$Orm->initMySql();
		
		$mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
		ORM_MySqlRecordSet::setBackend($mysqlbackend);
	} else {
		
		$mysqlbackend = ORM_MySqlRecordSet::getBackend();
		
		if (!isset($mysqlbackend))
		{
			ORM_MySqlRecordSet::setBackend(new ORM_MySqlBackend($GLOBALS['babDB']));
		}
		
	}
}


/**
 * @return workschedules_Controller
 */
function workschedules_Controller()
{
    workschedules_loadOrm();
	require_once dirname(__FILE__) . '/controller.class.php';
	return bab_getInstance('workschedules_Controller');
}


/**
 * @return bool
 */
function workschedules_isManager()
{
	return bab_isAccessValid('workschedules_manager_groups', 1);
}





function workschedules_PeriodModified($id_user = false)
{
	require_once $GLOBALS['babInstallPath'].'utilit/eventperiod.php';
	
	$event = new bab_eventPeriodModified(false, false, $id_user);
	$event->types = BAB_PERIOD_WORKING | BAB_PERIOD_NONWORKING;
	
	bab_fireEvent($event);
}



/**
 * 
 * @param Array $data
 * @param string $startDate
 * @param string $endDate
 * @throws ErrorException
 * @return boolean|string
 */
function workschedules_getRRuleString(Array $data, $startDate, $endDate)
{
	//var_dump($data);
	$rrule = array();
	if(isset($data['repeat']) && $data['repeat'] == 1)
	{

		$startDate = bab_mktime($startDate);
		$endDate = bab_mktime($endDate);
		
		
		$duration = $endDate - $startDate;
	
		switch( $data['repeat_frequency'] )
		{
			case 'weekly':
				
				if( empty($data['repeat_n_w']))
				{
					$data['repeat_n_w'] = 1;
				}
				
				if( $duration > 24 * 3600 * 7 * $data['repeat_n_w'])
				{
					throw new ErrorException(bab_translate("The duration of the event must be shorter than how frequently it occurs"));
					return false;
				}
				
				$rrule[]= 'INTERVAL='.(int) $data['repeat_n_w'];
	
				if( !isset($data['repeat_wd']) )
				{
					// no week day specified, reapeat event every week
					$rrule[]= 'FREQ=WEEKLY';
				}
				else
				{
					$rrule[]= 'FREQ=WEEKLY';
					$weekDay = array();
					foreach($data['repeat_wd'] as $k => $v){
						if($v != "0"){
							$weekDay[] = $v;
						}
					}
					 
					// BYDAY : add list of weekday    = "SU" / "MO" / "TU" / "WE" / "TH" / "FR" / "SA"
					$rrule[] = 'BYDAY='.implode(',', $weekDay);
				}
	
				break;
	
	
			case 'monthly':
				if( empty($data['repeat_n_m']))
				{
					$data['repeat_n_m'] = 1;
				}
				
				if( $duration > 24*3600*28*$data['repeat_n_m'])
				{
					throw new ErrorException(bab_translate("The duration of the event must be shorter than how frequently it occurs"));
					return false;
				}
	
				$rrule[]= 'INTERVAL='.$data['repeat_n_m'];
				$rrule[]= 'FREQ=MONTHLY';
				break;
	
			case 'yearly': /* yearly */
				if( empty($data['repeat_n_y']))
				{
					$data['repeat_n_y'] = 1;
				}
	
				if( $duration > 24*3600*365*$data['repeat_n_y'])
				{
					throw new ErrorException(bab_translate("The duration of the event must be shorter than how frequently it occurs"));
					return false;
				}
				$rrule[]= 'INTERVAL='.$data['repeat_n_y'];
				$rrule[]= 'FREQ=YEARLY';
				break;
	
			case 'daily': /* daily */
				if( empty($data['repeat_n_d']))
				{
					$data['repeat_n_d'] = 1;
				}
				if( $duration > 24*3600*$data['repeat_n_d'] )
				{
					throw new ErrorException(bab_translate("The duration of the event must be shorter than how frequently it occurs"));
					return false;
				}
				$rrule[]= 'INTERVAL='.$data['repeat_n_d'];
				$rrule[]= 'FREQ=DAILY';
				break;
		}
	
		if (isset($data['repeat_end_cb']) && $data['repeat_end_cb'] == 1)
		{
			require_once $GLOBALS['babInstallPath'].'utilit/evtincl.php';
			$repeatDate = explode('-',$data['repeat_end_date']);
			$data['until'] = array(
				'year'	=> (int) $repeatDate[2],
				'month'	=> (int) $repeatDate[1],
				'day'	=> (int) $repeatDate[0]
			);
			$until = bab_event_posted::getDateTime($data['until']);
			$until->add(1);
		
			if( $until->getTimeStamp() < $endDate) {
				$e = new bab_SaveErrorException(bab_translate("Repeat date must be older than end date"));
				$e->redirect = false;
				throw $e;
				return false;
			}
		
		
			$rrule[] = 'UNTIL='.$until->getICal(true);
		}
		return implode(';',$rrule);
	}
	
	return '';
}


/**
 * get RRule Form array
 * 
 * @param string|boolean $string
 * 
 * @return array
 */
function workschedules_getRRuleForm($string)
{	
	$stringArray = explode(';',$string);
	$data = array();
	foreach($stringArray as $item)
	{
		$tempArray = explode('=', $item);
		if(isset($tempArray[0]) && isset($tempArray[1])){
			$data[$tempArray[0]] = $tempArray[1];
		}
	}
	
	$return = array();
	
	if(isset($data['FREQ'])){
		$return['repeat'] = 1;
		switch( $data['FREQ'] )
		{
			case 'WEEKLY':
				$return['repeat_n_w'] = $data['INTERVAL'];
				$return['repeat_frequency'] = 'weekly';
				
				if(isset($data['BYDAY']))
				{
					$daysWeek = explode(',', $data['BYDAY']);
					foreach($daysWeek as $v){
						if($v){
							$return['repeat_wd'][$v] = $v;
						}
					}
				}
				
				break;
				
			case 'MONTHLY':
				$return['repeat_n_m'] = $data['INTERVAL'];
				$return['repeat_frequency'] = 'monthly';
				break;
		
			case 'YEARLY': /* yearly */
				$return['repeat_n_y'] = $data['INTERVAL'];
				$return['repeat_frequency'] = 'yearly';
				break;
		
			case 'DAILY': /* daily */
				$return['repeat_n_d'] = $data['INTERVAL'];
				$return['repeat_frequency'] = 'daily';
				break;
		}
	
		if (isset($data['UNTIL']))
		{
			$return['repeat_end_cb'] = 1;
			
			require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
			
			$tempBabDate = BAB_DateTime::fromICal($data['UNTIL']);
			$tempBabDate->add(-1);
			
			$return['repeat_end_date'] = $tempBabDate->getIsoDate();
			
			$return['repeat'] = 1;
		}
	}
	
	return $return;
}