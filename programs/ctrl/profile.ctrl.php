<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/profile.ui.php';
require_once dirname(__FILE__).'/../set/rule.class.php';
require_once dirname(__FILE__).'/../set/profile.class.php';
require_once dirname(__FILE__).'/../set/userprofile.class.php';
require_once dirname(__FILE__).'/../set/user.class.php';

bab_functionality::includeOriginal('Icons');


/**
 *
 */
class workschedules_CtrlProfile extends workschedules_Controller
{

	private function getListsPage()
	{
		$W = bab_Widgets();
		$page = $W->BabPage();

		$page->addItemMenu('displayList', workschedules_translate('Profiles list'), $this->proxy()->displayList()->url());
		$page->addItemMenu('displayUsersList', workschedules_translate('Users work schedules'), $this->proxy()->displayUsersList()->url());

		return $page;
	}



	/**
	 * Display list of named profiles
	 * @throws Exception
	 * @return Widget_BabPage
	 */
	public function displayList()
	{

		if (!workschedules_isManager())
		{
			throw new bab_AccessException(workschedules_translate('Access denied'));
		}

		$W = bab_Widgets();
		$page = $this->getListsPage();
		
		$Icons = bab_functionality::get('Icons');
		/*@var $Icons Func_Icons */
		$Icons->includeCss();
		
		$set = new workschedules_ProfileSet();


		$res = $set->select($set->user->is(''));
		$res->orderAsc($set->name);

		$toolbar = $W->FlowLayout()
			->addClass('bab_toolbar')
			->addClass(Func_Icons::ICON_LEFT_16);

		$page->addItem($toolbar);
		$page->addStyleSheet('toolbar.css');

		$toolbar->addItem($W->Link(
			$W->Icon(workschedules_translate('Create a profile'), Func_Icons::ACTIONS_DOCUMENT_NEW ),
			$this->proxy()->edit()
		)->addClass('bab_toolbarItem'));


		$list = new workschedules_ProfileTableView;
		$list->setDataSource($res);
		$list->addDefaultColumns($set);
		$page->addItem($list);



		$page->setCurrentItemMenu(__FUNCTION__);
		return $page;
	}


	/**
	 * Display profiles associated to users
	 */
	public function displayUsersList()
	{
		if (!workschedules_isManager())
		{
			throw new bab_AccessException(workschedules_translate('Access denied'));
		}

		$W = bab_Widgets();
		$page = $this->getListsPage();
		
		$Icons = bab_functionality::get('Icons');
		/*@var $Icons Func_Icons */
		$Icons->includeCss();
		
		$set = new workschedules_UserSet();

		$res = $set->select();
		$res->orderAsc($set->username_copy);

		$toolbar = $W->FlowLayout()
		->addClass('bab_toolbar')
		->addClass(Func_Icons::ICON_LEFT_16);

		$page->addItem($toolbar);
		$page->addStyleSheet('toolbar.css');

		$toolbar->addItem($W->Link(
			$W->Icon(workschedules_translate('Add a user for profile definition'), Func_Icons::ACTIONS_DOCUMENT_NEW ),
			$this->proxy()->chooseUser()
		)->addClass('bab_toolbarItem'));


		$list = new workschedules_UserProfileTableView;
		$list->setDataSource($res);
		$list->addDefaultColumns($set);
		$page->addItem($list);



		$page->setCurrentItemMenu(__FUNCTION__);
		return $page;
	}


	public function chooseUser()
	{
		if (!workschedules_isManager())
		{
			throw new bab_AccessException(workschedules_translate('Access denied'));
		}

		$W = bab_Widgets();
		$page = $W->BabPage();

		$currentItemMenuLabel = workschedules_translate('Choose the user');

		$page->addItemMenu('displayList', workschedules_translate('Profiles list'), $this->proxy()->displayList()->url());
		$page->addItemMenu('displayUsersList', workschedules_translate('Users work schedules'), $this->proxy()->displayUsersList()->url());
		$page->addItemMenu('choose', $currentItemMenuLabel, $this->proxy()->chooseUser()->url());
		$page->setCurrentItemMenu('choose');

		$editor = new workschedules_UserSelector();
		$page->addItem($editor);

		return $page;
	}

	public function edit($profile = null, $user = null)
	{
		if (!workschedules_isManager())
		{
			throw new bab_AccessException(workschedules_translate('Access denied'));
		}

		$W = bab_Widgets();
		$page = $W->BabPage();

		$currentItemMenuLabel = workschedules_translate('Name of the profile');

		$page->addItemMenu('displayList', workschedules_translate('Profiles list'), $this->proxy()->displayList()->url());
		$page->addItemMenu('displayUsersList', workschedules_translate('Users work schedules'), $this->proxy()->displayUsersList()->url());
		$page->addItemMenu('edit', $currentItemMenuLabel, $this->proxy()->edit($profile, $user)->url());
		$page->setCurrentItemMenu('edit');

		$set = new workschedules_ProfileSet();

		if (empty($profile))
		{
			$profileRecord = null;
		} else {
			$profileRecord = $set->get($profile);
		}

		$editor = new workschedules_ProfileEditor($profileRecord, $user);
		$page->addItem($editor);

		return $page;
	}


	/**
	 * Delete a named profile
	 * @param array $profile
	 * @return boolean
	 */
	public function delete($profile = null)
	{
		// do not allow delete if associated to users
		if(is_array($profile) && isset($profile['id'])){
			$profile = $profile['id'];
		}
		$set = new workschedules_ProfileSet();
		$profileRecord = $set->get($profile);
		/*@var $profileRecord workschedules_Profile */

		$users = $profileRecord->selectUsers();
		if (0 !== $users->count() && 1 !== $users->count())
		{
			$e = new bab_SaveException(sprintf(workschedules_translate(
					'This profile cannot be deleted because there is %d user linked to it',
					'This profile cannot be deleted because there are %d users linked to it',
					$users->count()
					)
			, $users->count()));
			$e->redirect = false;

			throw $e;
		}


		if(!$profileRecord->user){
			$action = workschedules_Controller()->Profile()->displayList();
		}else{
			$action = workschedules_Controller()->Profile()->displayUsersList();
		}

		$profileRecord->delete();

		$action->location();
	}




	public function addUserProfile($user = null, $uprofile = null)
	{
		if (!workschedules_isManager())
		{
			throw new bab_AccessException(workschedules_translate('Access denied'));
		}

		$W = bab_Widgets();
		$page = $W->BabPage();

		$userSet = new workschedules_UserSet();
		$user = $userSet->get($userSet->user->is($user));
		if(!$user){
			throw new bab_AccessException(workschedules_translate('Access denied'));
		}

		$userName = bab_getUserName($user->user);

		if($uprofile){
			$currentItemMenuLabel = sprintf(workschedules_translate('Update share profile to %s'),$userName);
		}else{
			$currentItemMenuLabel = sprintf(workschedules_translate('Add a share profile to %s'),$userName);
		}

		$page->addItemMenu('displayList', workschedules_translate('Profiles list'), $this->proxy()->displayList()->url());
		$page->addItemMenu('displayUsersList', workschedules_translate('Users work schedules'), $this->proxy()->displayUsersList()->url());
		$page->addItemMenu('addUserProfile', $currentItemMenuLabel, $this->proxy()->addUserProfile($user->id, $uprofile)->url());
		$page->setCurrentItemMenu('addUserProfile');

		$editor = new workschedules_UserProfileEditor($user, $uprofile);
		$page->addItem($editor);

		return $page;
	}


	/**
	 * Workshedules options for a user viewed from the absences addon
	 * @param int $user
	 * @throws bab_AccessException
	 */
	public function userPopup($user)
	{
		if (!workschedules_isManager())
		{
			throw new bab_AccessException(workschedules_translate('Access denied'));
		}

		$W = bab_Widgets();
		$page = $W->BabPage()->setEmbedded(false);

		$set = new workschedules_UserProfileSet;
		$set->profile();
		$userprofiles = $set->select($set->user->is($user));
		$userprofiles->orderAsc($set->from);

		if (isset($userprofiles) && count($userprofiles))
		{
			$list = new workschedules_UserProfileUserTableView;
			$list->setDataSource($userprofiles);
			$list->addDefaultColumns($set);
			$page->addItem($list);
		}else{
			$page->setTitle(sprintf(workschedules_translate('This user %s do not currently use work schedules'), bab_getUserName($user)));
			//

		}



		return $page;
	}





	public function closePopup()
	{
		$W = bab_Widgets();
		$page = $W->BabPage()->setEmbedded(false);

		$page->addItem($W->Html('<script type="text/javascript"> window.close(); </script>'));

		return $page;
	}






	/**
	 *
	 * @param array $user
	 * @throws bab_AccessException
	 * @throws Exception
	 * @throws bab_SaveErrorException
	 */
	public function userSave($user = null)
	{
		if (!workschedules_isManager())
		{
			throw new bab_AccessException(workschedules_translate('Access denied'));
		}

		$set = new workschedules_UserSet();

		$userRecord = $set->get($set->user->is($user['user']));
		if ($userRecord)
		{
			throw new bab_SaveErrorException(workschedules_translate('This user already have a profile'));
			return false;
		}

		$userRecord = $set->newRecord();

		/*@var $userRecord workschedules_User */


		if (!isset($userRecord))
		{
			throw new Exception('Unexpected error, missing record');
		}


		if (empty($user['user']))
		{
			throw new bab_SaveErrorException(workschedules_translate('The user is mandatory'));
		}

		$userRecord->user = $user['user'];
		$userRecord->username_copy = bab_getUserName($user['user']);
		$userRecord->save();

		return true;
	}

	/**
	 *
	 * @param int $user
	 * @throws bab_AccessException
	 * @throws Exception
	 * @throws bab_SaveErrorException
	 */
	public function removeUser($user = null)
	{
		if (!workschedules_isManager())
		{
			throw new bab_AccessException(workschedules_translate('Access denied'));
		}

		$set = new workschedules_UserSet();

		$userRecord = $set->get($set->user->is($user));
		if (!$userRecord)
		{
			throw new bab_SaveErrorException(workschedules_translate('This user can not be found'));
			return false;
		}

		$upset = new workschedules_UserProfileSet();
		$upset->delete($upset->user->is($user));

		$pset = new workschedules_ProfileSet();
		$pset->delete($pset->user->is($user));

		$set->delete($set->user->is($user));

		$this->proxy()->displayUsersList()->location();
	}






	/**
	 *
	 * @param unknown_type $profile
	 * @throws bab_AccessException
	 * @throws Exception
	 * @throws bab_SaveErrorException
	 */
	public function save($profile = null)
	{
		if (!workschedules_isManager())
		{
			throw new bab_AccessException(workschedules_translate('Access denied'));
		}

		$set = new workschedules_ProfileSet();

		if (empty($profile['id']))
		{
			$profileRecord = $set->newRecord();
			$newRecord = true;
		} else {
			$profileRecord = $set->get($profile['id']);
			$newRecord = false;
		}

		/*@var $profileRecord workschedules_Profile */


		if (!isset($profileRecord))
		{
			throw new Exception('Unexpected error, missing record');
		}

		$profile['name'] = trim($profile['name']);
		if (empty($profile['name']))
		{
			$e = new bab_SaveErrorException(workschedules_translate('The profile name is mandatory'));
			$e->redirect = false;
			throw $e;
		}

		if(isset($profile['user'])){
			$profileRecord->user = $profile['user'];

		}
		$profileRecord->name = $profile['name'];
		$profileRecord->week_duration = $profile['week_duration'];

		$profileRecord->modifiedOn = date('Y-m-d H:i:s');

		$upset = new workschedules_UserProfileSet();
		if(isset($profile['user']) && $profile['user']){
			$up = $upset->newRecord();
			$up->user = $profile['user'];
			if($profileRecord->id){
				$up = $upset->get($upset->user->is($profile['user'])->_AND_($upset->profile->is($profileRecord->id)));
			}

			$from = $upset->from->input($profile['from']);
			$to = $upset->to->input($profile['to']);

			$up->from = $from;
			$up->to = $to;
			$up->checkPeriod();
		}

		$profileRecord->save();

		if(isset($profile['user']) && $profile['user']){
			$up->profile = $profileRecord->id;
			$up->save();
		}

		bab_getInstance('babBody')->addNextPageMessage(sprintf(workschedules_translate('The profile %s has been saved, you can add rules to define the working days'), $profileRecord->getName()));


		$this->proxy()->display($profileRecord->id)->location();
	}


	/**
	 *
	 * @param int $profile
	 * @throws Exception
	 */
	public function display($profile = null)
	{
		if (!workschedules_isManager())
		{
			throw new bab_AccessException(workschedules_translate('Access denied'));
		}

		$set = new workschedules_ProfileSet();
		$profileRecord = $set->get($profile);


		if (!isset($profileRecord))
		{
			throw new Exception(workschedules_translate('This profile does not exists'));
		}

		$W = bab_Widgets();
		$page = $W->BabPage();
		
		$Icons = bab_functionality::get('Icons');
		/*@var $Icons Func_Icons */
		$Icons->includeCss();

		$page->addItemMenu('displayList', workschedules_translate('Profiles list'), workschedules_Controller()->Profile()->displayList()->url());
		$page->addItemMenu('displayUsersList', workschedules_translate('Users work schedules'), workschedules_Controller()->Profile()->displayUsersList()->url());
		$page->addItemMenu('displayRuleUsersList', workschedules_translate('Profile schedules'), workschedules_Controller()->Profile()->display($profile)->url());

		$page->setCurrentItemMenu('displayRuleUsersList');

		$frame = new workschedules_ProfileFullFrame($profileRecord);
		$page->addItem($frame);

		return $page;
	}




	/**
	 * Edit form to change profile associated to a user
	 */
// 	public function editUserProfile($user = null)
// 	{
// 		if (!workschedules_isManager())
// 		{
// 			throw new bab_AccessException(workschedules_translate('Access denied'));
// 		}


// 		$W = bab_Widgets();
// 		$page = $W->BabPage();


// 		if (isset($user))
// 		{
// 			if (!bab_getUserName($user))
// 			{
// 				$page->addError(workschedules_translate('The user does not exists, this row can be deleted'));
// 			}

// 			$set = new workschedules_UserProfileSet();
// 			$userProfileRecord = $set->get($set->user->is($user));
// 			/* @var $userProfileRecord workschedules_UserProfile */
// 		} else {
// 			$userProfileRecord = null;
// 		}


// 		$page->addItemMenu('displayList', workschedules_translate('Profiles list'), $this->proxy()->displayList()->url());
// 		$page->addItemMenu('displayUsersList', workschedules_translate('Users work schedules'), $this->proxy()->displayUsersList()->url());
// 		$page->addItemMenu('editUserProfile', workschedules_translate('User profile selection'), $this->proxy()->editUserProfile($user)->url());

// 		$editor = new workschedules_UserProfileEditor($userProfileRecord);
// 		if (!isset($userProfileRecord))
// 		{
// 			// create new profile
// 			$editor->setHiddenValue('userprofile[user]', $user);
// 		}
// 		$page->addItem($editor);
// 		$page->setCurrentItemMenu(__FUNCTION__);


// 		$workingHours = bab_functionality::get('WorkingHours');
// 		if (!($workingHours instanceof Func_WorkingHours_Workschedules))
// 		{
// 			$page->addError(workschedules_translate('The rules are not applicables because work schedules is not the selected default method in site options'));
// 		}

// 		return $page;
// 	}



	public function saveUserProfile($userprofile = null)
	{


		if (!workschedules_isManager())
		{
			throw new bab_AccessException(workschedules_translate('Access denied'));
		}


		if (empty($userprofile['profile']))
		{
			$e = new bab_SaveException(workschedules_translate('You must select a profile'));
			$e->redirect = false;
			throw $e;
		}

		$UserProfileSet = new workschedules_UserProfileSet();
		if(!$userprofile['id']){
			$userProfileRecord = $UserProfileSet->newRecord();
		}else{
			$userProfileRecord = $UserProfileSet->get($userprofile['id']);
		}

		//user creation

		$userProfileRecord->user = $userprofile['user'];

		$userProfileRecord->profile = $userprofile['profile'];

		$from = $UserProfileSet->from->input($userprofile['from']);
		$to = $UserProfileSet->to->input($userprofile['to']);

		$userProfileRecord->from = $from;
		$userProfileRecord->to = $to;
		$userProfileRecord->save();

		bab_getInstance('babBody')->addNextPageMessage(sprintf(workschedules_translate('%s profile modified'), bab_getUserName($userProfileRecord->user)));
		return true;
	}


	/**
	 *
	 */
	public function deleteUserProfile($userprofile = null)
	{
		if (!workschedules_isManager())
		{
			throw new bab_AccessException(workschedules_translate('Access denied'));
		}


		if ($userprofile['id'] == null)
		{
			$e = new bab_SaveException(workschedules_translate('You must select a profile'));
			$e->redirect = false;
			throw $e;
		}

		$UserProfileSet = new workschedules_UserProfileSet();
		$userProfileRecord = $UserProfileSet->get($userprofile['id']);
		$userProfileRecord->clear();

		$UserProfileSet->delete($UserProfileSet->id->is($userProfileRecord->id));

		bab_getInstance('babBody')->addNextPageMessage(sprintf(workschedules_translate('%s profile modified'), bab_getUserName($userProfileRecord->user)));
		$this->proxy()->displayUsersList()->location();
	}




	public function cancel()
	{
		$this->proxy()->displayList()->location();
	}
}


