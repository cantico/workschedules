<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/rule.ui.php';
require_once dirname(__FILE__).'/../set/rule.class.php';
require_once dirname(__FILE__).'/../set/profile.class.php';


/**
 * 
 */
class workschedules_CtrlRule extends workschedules_Controller
{	
	public function edit($rule = null, $profile = null)
	{
		if (!workschedules_isManager())
		{
			throw new Exception(workschedules_translate('Access denied'));
		}
		
		
		
		$W = bab_Widgets();
		$page = $W->BabPage();
		$page->addStyleSheet('addons/workschedules/workschedules.css');
		
		$Icons = bab_functionality::get('Icons');
		/*@var $Icons Func_Icons */
		$Icons->includeCss();
		
		if($rule){
			$ruleSet = new workschedules_RuleSet();
			$rule = $ruleSet->get($rule);
		}
		
		if($rule instanceof workschedules_Rule){
			$editor = new workschedules_RuleEditor($rule);
			$profile = $rule->profile;
		}else{
			$editor = new workschedules_RuleEditor();
		}
		
		$postRule = bab_pp('rule');
		if($profile === null && isset($postRule['profile'])){
			$profile = $postRule['profile'];
		}
		$editor->setProfile($profile);
		
		$page->addItemMenu('displayUsersList', workschedules_translate('Rule list'), workschedules_Controller()->Profile()->display($profile)->url());
		$page->addItemMenu('displayRuleUsersList', workschedules_translate('Rule edition'), '');
		
		$page->setCurrentItemMenu('displayRuleUsersList');
		
		$page->addItem($editor);
		
		return $page;
	}
	
	
	public function save($rule = array())
	{
		if (!workschedules_isManager())
		{
			throw new Exception(workschedules_translate('Access denied'));
		}
		
		
		
		
		$ruleSet = new workschedules_RuleSet();
		
		$ruleRecord = $ruleSet->newRecord();
		
		if(isset($rule['id'])){
			$ruleRecord->id = $rule['id'];
		} 
		
		$W = bab_Widgets();
		$datePicker = $W->DatePicker();
		
		
		$dateTest = explode('-', $rule['period']);
		if(!isset($dateTest[2]) || $dateTest[2] < 1990 || $dateTest[2] > 2200 || $dateTest[1] > 12 || $dateTest[1] < 1 || $dateTest[0] < 1 || $dateTest[0] > 31){
			$e = new bab_SaveErrorException(workschedules_translate('The date is incorret.'));
			$e->redirect = false;
			throw $e;
		}
		
		if($rule['timebegin'] >= $rule['timeend'])
		{
			$e = new bab_SaveErrorException(workschedules_translate('This period is empty.'));
			$e->redirect = false;
			throw $e;
		}
		
		
		if ($rule['exclude'])
		{
			// verifier qu'il existe au moins une regle qui ajoute des periodes travailles avant d'exclure
			
			$test = $ruleSet->get($ruleSet->profile->is($rule['profile'])
					->_AND_($ruleSet->exclude->is(0))
					->_AND_($ruleSet->id->is($ruleRecord->id)->_NOT())
				);
			
			if (null === $test)
			{
				$e = new bab_SaveErrorException(workschedules_translate('You cannot exclude periods from the workings periods since there are no working periodes configured'));
				$e->redirect = false;
				throw $e;
			}
		}
		
		
		
		$ruleRecord->begin = $datePicker->getISODate($rule['period']);
		$ruleRecord->begin.= ' ' . $rule['timebegin'];
		
		$ruleRecord->end = $datePicker->getISODate($rule['period']);
		$ruleRecord->end.= ' ' . $rule['timeend'];
		
		$ruleRecord->rrule = workschedules_getRRuleString($rule, $ruleRecord->begin, $ruleRecord->end);
		$ruleRecord->exclude = $rule['exclude'];
		$ruleRecord->profile = $rule['profile'];
		
		$ruleRecord->save();

		$profileSet = new workschedules_ProfileSet();
		$profileRecord = $profileSet->get($ruleRecord->profile);
		$profileRecord->modifiedOn = date('Y-m-d H:i:s');
		$profileRecord->save();
		

		
		
		
		workschedules_Controller()->Profile()->display($ruleRecord->profile)->location();
	}
	
	
	
	
	public function delete($rule = null)
	{	
		$ruleSet = new workschedules_RuleSet();
		$ruleSet->delete($ruleSet->id->is($rule['id']));
		
		workschedules_PeriodModified();
		
		return true;
	}
	
}


