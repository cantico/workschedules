<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
include "base.php";

bab_functionality::includeOriginal('WorkingHours');
require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__).'/RRule.php';






class Func_WorkingHours_Workschedules extends Func_WorkingHours
{
	public function getDescription()
	{
		return workschedules_translate('Work schedules periods planification');
	}
	
	
	private function loadOrm()
	{
	    workschedules_loadOrm();
	    require_once dirname(__FILE__).'/set/rule.class.php';
	    require_once dirname(__FILE__).'/set/profile.class.php';
	    require_once dirname(__FILE__).'/set/userprofile.class.php';
	}


	/**
	 * Get the working periods between two date
	 * @param BAB_DateTime $begin
	 * @param BAB_DateTime $end
	 *
	 * @return array <bab_WorkingPeriod>
	 *
	 */
	public function selectPeriods($id_user, BAB_DateTime $begin, BAB_DateTime $end)
	{
		$this->loadOrm();
		
		$userProfileSet = new workschedules_UserProfileSet();
		$userProfileSet->profile();
		$userProfiles = $userProfileSet->select(
		    $userProfileSet->user->is($id_user)
		    ->_AND_($userProfileSet->profile->id->greaterThan(0))
		);
		
		/* @var $userProfile workschedules_UserProfile */
		
		if($userProfiles->count() == 0) {
			$func = bab_functionality::get('WorkingHours/Ovidentia');
			return $func->selectPeriods($id_user, $begin, $end);
		}
		
		$arr = array();
		foreach($userProfiles as $userProfile){
			$from = $userProfile->from;
			$to = $userProfile->to;
			if($to == '0000-00-00'){
				$to = $end->getIsoDate();
			}
			
			$periods = $userProfile->profile->getRulesPeriods($begin, $end);
			
			foreach($periods as $p) {
				if(BAB_DateTime::periodIntersect($from . ' 00:00:00', $to . ' 23:59:59', $p['start']->getIsoDateTime(), $p['end']->getIsoDateTime())){
					$arr[] = new bab_WorkingPeriod(
						$p['start']->getIsoDateTime(),
						$p['end']->getIsoDateTime()
					);
				}
			}
		}
		
		
		
		
		// bab_debug($begin->getIsoDateTime().' '.$end->getIsoDateTime());
		return $arr;
	}
	
	
	
	/**
	 * Test if the functionality has default settings (always return periods)
	 * @throws Exception
	 *
	 * @return bool
	 */
	public function hasDefaultSettings()
	{
		return false;
	}
	
	/**
	 * Test if the functionality has settings for the user
	 * @param int $id_user
	 * @throws Exception
	 *
	 * @return bool
	 */
	public function hasUserSettings($id_user)
	{
	    $this->loadOrm();
	    
		$upSet = new workschedules_UserProfileSet();
		$upSet->profile();
		$userProfile = $upSet->get($upSet->user->is($id_user));
		
		return isset($userProfile);
	}
	
	
	/**
	 * Get url for user setting in a popup
	 * return null if popup not available
	 *
	 * @param int $id_user
	 * @return string | null
	 */
	public function getUserSettingsPopupUrl($id_user)
	{
		
		if (!workschedules_isManager())
		{
			return null;
		}
		
		return workschedules_Controller()->Profile()->userPopup($id_user)->url();
	}
	
	
	
	
	/**
	 * Get url for profile list
	 * return null if page is not available
	 * 
	 * @return string | null
	 */
	public function getProfileListUrl()
	{
		if (!workschedules_isManager())
		{
			return null;
		}
		
		return workschedules_Controller()->Profile()->displayList()->url();
	}
	
	/**
	 * Get the list of profiles selectable for a user
	 * 
	 * @return array
	 */
	public function getProfiles($id_user)
	{
	    $this->loadOrm();
	    
	    $set = new workschedules_ProfileSet();
	    $res = $set->select($set->user->is(0)->_OR_($set->user->is($id_user)))->orderAsc($set->name);
	    
	    $arr = array();
	    foreach($res as $profile) {
	        $arr[(int) $profile->id] = $profile->name;
	    }
	    
	    return $arr;
	}
	
	/**
	 * @return workschedules_UserProfileSet
	 */
	public function userProfileSet()
	{
	    $this->loadOrm();
	    
	    return new workschedules_UserProfileSet();
	}
	
	/**
	 * Get the profiles associated to a user
	 * 
	 * @return workschedules_UserProfile[]
	 */
	public function getUserProfiles($id_user)
	{
	    $this->loadOrm();
	    
	    $profileSet = new workschedules_UserProfileSet();
	    $profileSet->profile();
	    $res = $profileSet->select($profileSet->user->is($id_user));
	    
	    $res->orderAsc($profileSet->from);
	    $res->orderAsc($profileSet->to);
	    
	    return $res;
	}

	/**
	 * @return workschedules_Profile
	 */
	public function getCurrentProfile($id_user)
	{
	    $this->loadOrm();
	    
	    $now = date('Y-m-d H:i:s');
	    $notSet = '0000-00-00 00:00:00';
	    
	    $upSet = new workschedules_UserProfileSet();
	    $upSet->profile();
	    $userProfile = $upSet->get(
	        $upSet->user->is($id_user)
	        ->_AND_($upSet->from->lessThanOrEqual($now)->_OR_($upSet->from->is($notSet)))
	        ->_AND_($upSet->to->greaterThanOrEqual($now)->_OR_($upSet->to->is($notSet)))
	    );
	    
	    if (!$userProfile) {
	        return null;
	    }
	    
	    return $userProfile->profile;
	}
}
